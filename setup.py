#!/usr/bin/env python

from distutils.core import setup
from efl.utils.setup import build_extra, build_fdo, uninstall


setup(
    name='espionage',
    version='1.0',
    description='D-Bus inspector',
    long_description='Espionage is a complete D-Bus inspector',
    url='https://phab.enlightenment.org/w/projects/espionage/',
    license="GNU GPL",
    author='Dave Andreoli',
    author_email='dave@gurumeditation.it',
    packages=['espionage'],
    requires=['efl (>=1.13)', 'dbus'],
    provides=['espionage'],
    scripts=['bin/espionage'],
    package_data={
        'espionage': ['themes/*/*'],
    },
    cmdclass={
        'build': build_extra,
        'build_fdo': build_fdo,
        'uninstall': uninstall,
    },
    command_options={
        'install': {'record': ('setup.py', 'installed_files.txt')}
    },
)
